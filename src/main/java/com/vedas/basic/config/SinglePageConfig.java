package com.vedas.basic.config;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SinglePageConfig implements ErrorController {

  @RequestMapping("/error")
  public String handleError(HttpServletRequest request, RedirectAttributes model) {
    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
    if (status != null) {
      Integer statusCode = Integer.valueOf(status.toString());
      if (statusCode != 404) {
        model.addAttribute("error", statusCode);
        return "redirect:/";
      }
    }
    return "index";
  }

  @Override
  public String getErrorPath() {
    return "/error";
  }
}
