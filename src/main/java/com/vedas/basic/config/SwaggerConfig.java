package com.vedas.basic.config;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.beans.factory.annotation.Value;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${app.header}")
    String header;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build().apiInfo(apiEndPointsInfo())
                .globalOperationParameters(
                        Lists.newArrayList(new ParameterBuilder()
                                .name(header)
                                .description("Description of header")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .required(false)
                                .build(),
                                new ParameterBuilder()
                                        .name("Accept-Language")
                                        .description("Accept-Language header")
                                        .modelRef(new ModelRef("string"))
                                        .parameterType("header")
                                        .defaultValue("en")
                                        .required(true)
                                        .build()
                        ));
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().version("2.0").title("Spring Boot REST API")
                .description("Restaurant Management System API")
                .contact(new Contact("Nikesh Maharjan", "www.maharjann.com.np", "maharjannik98@gmail.com"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}
