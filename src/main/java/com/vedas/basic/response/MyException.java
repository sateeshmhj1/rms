package com.vedas.basic.response;

import com.vedas.restaurant.enums.ErrorCode;
import lombok.Data;

@Data
public class MyException extends Exception {

    private ErrorCode errorCode;
    private Object data;

    public MyException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public MyException(ErrorCode errorCode, Object data) {
        this.errorCode = errorCode;
        this.data = data;
    }

}
