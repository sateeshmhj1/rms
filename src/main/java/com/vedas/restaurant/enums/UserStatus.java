package com.vedas.restaurant.enums;

public enum UserStatus {
    ACTIVE, DEACTIVE
}
