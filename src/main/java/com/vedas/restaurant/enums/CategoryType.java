package com.vedas.restaurant.enums;

public enum CategoryType {
  BREAKFAST, LUNCH, DINNER
}
