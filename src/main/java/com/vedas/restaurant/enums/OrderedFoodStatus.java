package com.vedas.restaurant.enums;

public enum OrderedFoodStatus {
  PLACED, COOKING, FINISHED, DELIVERED, CANCELED
}
