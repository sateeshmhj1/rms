package com.vedas.restaurant.enums;

public enum OrderStatus {
  PLACED, FINISHED, CANCELED, PAYED
}
