package com.vedas.restaurant.enums;

public enum UserRole {
    ROLE_WAITER, ROLE_CHEF, ROLE_ADMIN, ROLE_USER, ROLE_CASHIER
}
