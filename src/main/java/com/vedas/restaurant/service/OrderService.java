package com.vedas.restaurant.service;

import com.vedas.restaurant.dao.projection.IDs;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.dto.response.OrderResponseDto;
import com.vedas.restaurant.enums.OrderStatus;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface OrderService {

  OrderResponseDto placeOrder(Integer tableNumber) throws Exception;

  Page<OrderResponseDto> getOrders(Pageable pg) throws Exception;

  Page<IDs> getAllOrders(String status, Pageable pg) throws Exception;

  OrderResponseDto getById(int id) throws  Exception;

  List<CartResponseDto> getCartFoodByStatus(OrderedFoodStatus status) throws Exception;

  void changeOrderStatus(Integer id, OrderStatus status) throws Exception;
}
