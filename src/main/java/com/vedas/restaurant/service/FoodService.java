package com.vedas.restaurant.service;

import com.vedas.restaurant.dto.request.FavouriteFoodRequestDto;
import com.vedas.restaurant.dto.request.FoodRequestDto;
import com.vedas.restaurant.dto.response.FoodResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodService {

  public void addFood(FoodRequestDto dto) throws Exception;

  public Page<FoodResponseDto> getFoods(Pageable page) throws Exception;

  public void changeFoodStatus(Integer id, Boolean status) throws Exception;

  public void updateFood(Integer id, FoodRequestDto dto) throws Exception;

  public FoodResponseDto favouriteFood(Integer id, FavouriteFoodRequestDto dto) throws Exception;

  public List<FoodResponseDto> getFavouriteFood() throws Exception;

  void rateFood(int foodId, int rating) throws Exception;

  List<FoodResponseDto> getFrequentFood() throws Exception;
}
