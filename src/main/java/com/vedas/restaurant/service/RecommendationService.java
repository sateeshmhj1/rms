package com.vedas.restaurant.service;

import com.vedas.restaurant.dto.response.FoodResponseDto;

import java.util.List;

public interface RecommendationService {

  List<FoodResponseDto> getRecommendedFoods() throws Exception;

}
