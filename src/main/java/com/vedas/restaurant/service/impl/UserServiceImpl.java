package com.vedas.restaurant.service.impl;

import com.vedas.basic.response.MyException;
import com.vedas.basic.security.OnlineUser;
import com.vedas.restaurant.dao.PhoneNumberRepository;
import com.vedas.restaurant.dao.RatingRepository;
import com.vedas.restaurant.dao.UserProviderRepository;
import com.vedas.restaurant.dao.UserRepository;
import com.vedas.restaurant.database.*;
import com.vedas.restaurant.dto.*;
import com.vedas.restaurant.dto.response.RatingResponseDto;
import com.vedas.restaurant.dto.response.UserIdResponseDto;
import com.vedas.restaurant.dto.response.UserResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.UserRole;
import com.vedas.restaurant.enums.UserStatus;
import com.vedas.restaurant.mapper.UserMapper;
import com.vedas.restaurant.service.EmailService;
import com.vedas.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userDao;

  @Autowired
  public EmailService emailService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private UserMapper mapper;

  @PostConstruct
  public void admin() {
    List<Users> list = userDao.findByUserRole(UserRole.ROLE_ADMIN);
    if (list.isEmpty()) {
      Users user = new Users();
      user.setUsername("admin");
      user.setPassword(passwordEncoder.encode("String123"));
      user.setUserRole(UserRole.ROLE_ADMIN);
      user.setUserStatus(UserStatus.ACTIVE);
      user.setLastPasswordResetDate(new Date());
      user.setLastLoginDate(new Date());
      userDao.save(user);
      Users user1 = new Users();
      user1.setUsername("user");
      user1.setPassword(passwordEncoder.encode("String123"));
      user1.setUserRole(UserRole.ROLE_USER);
      user1.setUserStatus(UserStatus.ACTIVE);
      user1.setLastPasswordResetDate(new Date());
      user1.setLastLoginDate(new Date());
      userDao.save(user1);
      Users user2 = new Users();
      user2.setUsername("user1");
      user2.setPassword(passwordEncoder.encode("String123"));
      user2.setUserRole(UserRole.ROLE_USER);
      user2.setUserStatus(UserStatus.ACTIVE);
      user2.setLastPasswordResetDate(new Date());
      user2.setLastLoginDate(new Date());
      userDao.save(user2);
      Users user3 = new Users();
      user3.setUsername("user2");
      user3.setPassword(passwordEncoder.encode("String123"));
      user3.setUserRole(UserRole.ROLE_USER);
      user3.setUserStatus(UserStatus.ACTIVE);
      user3.setLastPasswordResetDate(new Date());
      user3.setLastLoginDate(new Date());
      userDao.save(user3);

    }
  }

  @Override
  public Users findCurrentUser() throws Exception {
    try {
      return userDao.findById(getId()).orElse(null);
    } catch (Exception ex) {
      return null;
    }
  }

  private Integer getId() throws Exception {
    OnlineUser user = (OnlineUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return user.getId();
  }

  @Override
  public Page<UserIdResponseDto> findByUserRole(UserRole role, Pageable pg) throws Exception {
    Page<Users> page = userDao.findByUserRole(role, pg);
    List<UserIdResponseDto> list = mapper.toUserResponseList((List<Users>) page.getContent());
    Page<UserIdResponseDto> pageResp = new PageImpl<>(list, page.getPageable(), page.getTotalElements());
    return pageResp;
  }

  @Value("${app.sms-expiration}")
  Long expiration;

  @Override
  public Users createUser(UserCreateDto dto) throws Exception {
    Long max = System.currentTimeMillis();
    phoneNumberRepository.findByPhoneNumberAndVerificationCodeAndSignupDateBetween(dto.getUsername(),
        dto.getVerificationCode(), max - expiration, max).orElseThrow(() -> {
      return new MyException(ErrorCode.V008);
    });
    Users newUser = mapper.toUser(dto);
    newUser.setPassword(passwordEncoder.encode(dto.getPassword()));
    newUser.setUserRole(UserRole.ROLE_USER);
    newUser.setUserStatus(UserStatus.ACTIVE);
    newUser.setLastLoginDate(new Date());
    newUser.setLastPasswordResetDate(new Date());
    userDao.save(newUser);
    return newUser;
  }

  @Override
  public void updateUser(UserCreateDto dto, Integer userId) throws Exception {
    Users user = userDao.findById(userId).orElseThrow(() -> {
      return new MyException(ErrorCode.U001);
    });
    user = mapper.toUser(dto);
    user.setPassword(passwordEncoder.encode(dto.getPassword()));
    user.setUserRole(UserRole.ROLE_USER);
    user.setUserStatus(UserStatus.ACTIVE);
    user.setLastLoginDate(new Date());
    user.setLastPasswordResetDate(new Date());
    userDao.save(user);
  }

  @Override
  public Users findByUsername(String username) throws Exception {
    Users user = userDao.findByUsername(username).orElseThrow(()->{
      return new MyException(ErrorCode.V006);
    });
    return user;
  }

  @Override
  public void forgotPassword(ForgetDto dto) throws Exception {
    Users user = userDao.findByUsernameOrEmail(dto.getUsername(), dto.getUsername()).orElseThrow(() -> {
      return new MyException(ErrorCode.V006);
    });
    Random rNo = new Random();
    int verificationCode = rNo.nextInt((9999 - 1000) + 1) + 1000;
    user.setVerificationCode(verificationCode);
    userDao.save(user);
    emailService.sendEmailMessage(user.getUsername(), "Confirm your email ",
        "Verification code: " + verificationCode);
  }

  @Override
  public Users verifyUser(VerificationDto dto) throws Exception {
    Users user = userDao.findByUsernameAndVerificationCode(dto.getUsername(), dto.getVerificationCode()).orElseThrow(() -> {
      return new MyException(ErrorCode.V006);
    });
    return user;
  }

  @Override
  public void resetPassword(ResetPasswordDto dto) throws Exception {
    if (dto.getPassword() == null ? dto.getRepassword() == null
        : dto.getPassword().equals(dto.getRepassword())) {
      Users user = verifyUser(dto);
      user.setPassword(passwordEncoder.encode(dto.getPassword()));
      userDao.save(user);
    } else {
      throw new MyException(ErrorCode.V010);
    }
  }

  @Autowired
  PhoneNumberRepository phoneNumberRepository;

  @Override
  public void sendVerificationCode(String number) throws Exception {
    Users user = userDao.findByUsername(number).orElseThrow(()->{
      return new MyException(ErrorCode.U001);
    });
    if (user == null) {
      PhoneNumbers phoneNumber = phoneNumberRepository.findById(number).orElse(new PhoneNumbers());
      //send code
      phoneNumber.setPhoneNumber(number);
      Random rNo = new Random();
      int verificationCode = rNo.nextInt((9999 - 1000) + 1) + 1000;
      phoneNumber.setVerificationCode(verificationCode);
      phoneNumber.setSignupDate(System.currentTimeMillis());
      phoneNumberRepository.save(phoneNumber);
    } else {
      throw new MyException(ErrorCode.U004);
    }
  }

  @Override
  public Page<UserResponseDto> getStaffs(Pageable pg) throws Exception {
    Page<Users> userList = userDao.findByUserRoleNot(UserRole.ROLE_USER, pg);
    List<UserResponseDto> responseList = mapper.toUserResponse(userList.getContent());
    return new PageImpl<>(responseList, userList.getPageable(), userList.getTotalElements());
  }

  @Override
  public void createStaff(StaffCreateDto dto) throws Exception {
    Users user = userDao.findByUsername(dto.getUsername()).orElse(null);
    if(user != null){
      throw new MyException(ErrorCode.U004);
    }
    user = mapper.toUser(dto);
    user.setPassword(passwordEncoder.encode(dto.getPassword()));
    user.setUserStatus(UserStatus.ACTIVE);
    user.setLastPasswordResetDate(new Date());
    user.setLastPasswordDate(new Date());
    userDao.save(user);
    UserDetails userDetails = mapper.toUserDetails(dto);
    userDetails.setId(user.getId());
    user.setUserDetails(userDetails);
    userDao.save(user);
  }

  @Override
  public void updateStaff(int id, StaffCreateDto dto) throws Exception {
    Users user = userDao.findById(id).orElseThrow(()->{
      return new MyException(ErrorCode.U001);
    });
    mapper.toUser(user, dto);
    mapper.toUserDetails(user.getUserDetails(), dto);
    user.setLastPasswordDate(new Date());
    userDao.save(user);
  }

  @Autowired
  RatingRepository ratingRepository;

  @Override
  public List<RatingResponseDto> getRatings() throws Exception {
    List<Ratings> ratings = ratingRepository.findByUserId(findCurrentUser());
    return mapper.toRatingResponseDto(ratings);
  }

  @Autowired
  UserProviderRepository providerRepository;

  @Override
  public String createUser(SocialUserDto data) throws Exception {
    Optional<UserProvider> userProvider = providerRepository.findByProviderId(data.getProviderId());

    String username = "";
    if (userProvider.isPresent()) {
      username = userProvider.get().getUserId().getId() + "";
    } else {
      Optional<Users> find = userDao.findByEmail(data.getEmail());
      Users newUser;
      if (find.isPresent()) {
        newUser = find.get();
      } else {
        newUser = createUsers(data.getEmail(), "", false, UserRole.ROLE_USER);
        newUser.setPhotoURL(data.getPhotoURL());
        UserDetails detail = new UserDetails();
        detail.setFullName(data.getFullName());
        detail.setId(newUser.getId());
        newUser.setUserDetails(detail);
        userDao.save(newUser);
      }
      UserProvider up = new UserProvider();
      up.setProvider(data.getProvider());
      up.setProviderId(data.getProviderId());
      up.setUserId(newUser);
      providerRepository.save(up);
      username = newUser.getId() + "";
    }
    return username;
  }

  private Users createUsers(String email, String password, Boolean status, UserRole role) throws Exception {
    Users newUser = new Users();
    Optional<Users> user = userDao.findByEmail(email);
    if (user.isPresent()) {
      throw new MyException(ErrorCode.U010);
    }
    newUser.setEmail(email);
    newUser.setPassword(passwordEncoder.encode(password));
    newUser.setUserRole(role);
    newUser.setUserStatus(UserStatus.ACTIVE);
    newUser.setLastLoginDate(new Date());
    newUser.setLastPasswordResetDate(new Date());
    userDao.save(newUser);
    return newUser;
  }
}
