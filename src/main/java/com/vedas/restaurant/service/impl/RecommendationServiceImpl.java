package com.vedas.restaurant.service.impl;

import com.vedas.restaurant.dao.FoodRepository;
import com.vedas.restaurant.dao.UserRepository;
import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.database.Ratings;
import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.dto.response.FoodResponseDto;
import com.vedas.restaurant.enums.UserRole;
import com.vedas.restaurant.mapper.FoodMapper;
import com.vedas.restaurant.service.RecommendationService;
import com.vedas.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RecommendationServiceImpl implements RecommendationService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  FoodRepository foodRepository;

  @Autowired
  UserService userService;

  @Autowired
  FoodMapper foodMapper;

  @Override
  public List<FoodResponseDto> getRecommendedFoods() throws Exception {
    Users user = userService.findCurrentUser();
    List<Foods> foodList = foodRepository.findByStatusIsTrue();
    Map<Foods, Double> recommendation = new HashMap<>();
    for (Foods food : foodList) {
      double recommendedFoods = getRecommendedFoods(user, food);
      recommendation.put(food, recommendedFoods);
    }
    List<Map.Entry<Foods, Double>> list = new ArrayList<>(recommendation.entrySet());
    list.sort(Map.Entry.comparingByValue());
    Collections.reverse(list);

    List<Foods> recommendedFood = new ArrayList<>();
    for (Map.Entry data : list) {
      recommendedFood.add((Foods) data.getKey());
    }
    return foodMapper.toFoodResponseDto(recommendedFood);
  }


  public double getRecommendedFoods(Users user, Foods food) throws Exception {
    ArrayList<Users> neighboorhoodUsers = getNeighboorhood(user, 1);
    return getPrediction(neighboorhoodUsers, food);
  }

  private double getPrediction(ArrayList<Users> neighboorhoodUsers, Foods food) {
    double totalRating = 0;
    int totalRatingDone = 0;
    for (Users user : neighboorhoodUsers) {
      int rating = getRatingOfFood(user, food);
      totalRating += rating;
      if (rating != -1) {
        totalRatingDone++;
      }
    }
    if (totalRatingDone != 0) {
      return totalRating / totalRatingDone;
    }
    return 0;
  }

  private int getRatingOfFood(Users user, Foods food) {
    for (Ratings rating : user.getRatings()) {
      if (rating.getFoodId().getId() == food.getId()) {
        return rating.getRating();
      }
    }
    return -1;
  }

  public ArrayList getNeighboorhood(Users user, int threshold) {
    List<Users> usersList = userRepository.findByUserRole(UserRole.ROLE_USER);
    ArrayList<Users> neighbours = new ArrayList<>();
    for (Users u : usersList) {
      if (u.getId() != user.getId()) {
        double similarity = calculateSimilarity(u, user);
        if (similarity < threshold) {
          neighbours.add(u);
        }
      }
    }
    return neighbours;
  }

  private double calculateSimilarity(Users other, Users user) {
    int foodInCommon = 0;
    double differenceSum = 0.0;
    for (Ratings r1 : user.getRatings()) {
      for (Ratings r2 : other.getRatings()) {
        Foods f1 = r1.getFoodId();
        Foods f2 = r2.getFoodId();
        if (f1.getId() == f2.getId()) {
          foodInCommon++;
          differenceSum += Math.abs(r1.getRating() - r2.getRating());
        }
      }
    }
    if (foodInCommon > 0) {
      return differenceSum / foodInCommon;
    }
    return Integer.MAX_VALUE;
  }
}