package com.vedas.restaurant.service.impl;

import com.vedas.basic.response.MyException;
import com.vedas.restaurant.dao.MenuCategoryRepository;
import com.vedas.restaurant.database.MenuCategory;
import com.vedas.restaurant.dto.request.MenuCategoryRequestDto;
import com.vedas.restaurant.dto.response.MenuCategoryResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.mapper.MenuCategoryMapper;
import com.vedas.restaurant.service.MenuCategoryService;
import com.vedas.restaurant.util.ImageUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuCategoryServiceImpl implements MenuCategoryService {

  @Autowired
  MenuCategoryRepository categoryRepository;

  @Autowired
  MenuCategoryMapper categoryMapper;

  @Autowired
  ImageUploadUtil imageUploadUtil;

  @Override
  public void addCategory(MenuCategoryRequestDto dto) throws Exception {
    MenuCategory category = categoryMapper.toMenuCategory(dto);
    if (dto.getCategoryId() != null) {
      MenuCategory parentCategory = categoryRepository.findById(dto.getCategoryId()).orElseThrow(() -> {
        return new MyException(ErrorCode.C001);
      });
      category.setCategoryId(parentCategory);
    }
    if (dto.getImage() != null) {
      String photoURL = imageUploadUtil.imageUpload(dto.getImage(), "category");
      category.setPhotoURL(photoURL);
    }
    category.setStatus(Boolean.FALSE);
    categoryRepository.save(category);
  }

  @Override
  public List<MenuCategoryResponseDto> getAllCategory() throws Exception {
    return categoryMapper.toMenuCategoryResponseDtoList(categoryRepository.findAll());
  }

  @Override
  public List<MenuCategoryResponseDto> getCategory() throws Exception {
    return categoryMapper.toMenuCategoryResponseDtoList(categoryRepository.findByStatusTrue());
  }

  @Override
  public void updteCategory(int id, MenuCategoryRequestDto dto) throws Exception {
    MenuCategory oldCategory = categoryRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.C001);
    });
    categoryMapper.toMenuCategory(oldCategory, dto);
    if (dto.getCategoryId() != null && dto.getCategoryId() != 0) {
      MenuCategory parentCategory = categoryRepository.findById(dto.getCategoryId()).orElseThrow(() -> {
        return new MyException(ErrorCode.C001);
      });
      oldCategory.setCategoryId(parentCategory);
    }
    if (dto.getImage() != null) {
      String photoURL = imageUploadUtil.imageUpload(dto.getImage(), "category");
      oldCategory.setPhotoURL(photoURL);
    }
    if (dto.getImage() == null && oldCategory.getPhotoURL() != null) {
      oldCategory.setPhotoURL(oldCategory.getPhotoURL());
    }
    oldCategory.setStatus(Boolean.FALSE);
    categoryRepository.save(oldCategory);
  }

  @Override
  public void deleteCategory(int id) throws Exception {
    MenuCategory category = categoryRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.C001);
    });
    categoryRepository.delete(category);
  }

  @Override
  public void changeStatus(int id, Boolean status) throws Exception {
    MenuCategory category = categoryRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.C001);
    });
    category.setStatus(status);
    categoryRepository.save(category);
  }

}
