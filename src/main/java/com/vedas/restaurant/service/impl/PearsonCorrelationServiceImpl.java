package com.vedas.restaurant.service.impl;

import com.vedas.restaurant.dao.FoodRepository;
import com.vedas.restaurant.dao.RatingRepository;
import com.vedas.restaurant.dao.UserRepository;
import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.database.Ratings;
import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.enums.UserRole;
import com.vedas.restaurant.service.PearsonCorrelationService;
import com.vedas.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PearsonCorrelationServiceImpl implements PearsonCorrelationService {

  @Autowired
  RatingRepository ratingRepository;

  @Autowired
  UserRepository userRepository;

  @Autowired
  FoodRepository foodRepository;

  public double[][] getRatingMatrix() {
    List<Users> allUsers = userRepository.findByUserRole(UserRole.ROLE_USER);
    List<Foods> allFood = foodRepository.findAll();
    double[][] ratingMatrix = new double[allUsers.size() + 1][allFood.size() + 1];
    for (double[] row : ratingMatrix) {
      Arrays.fill(row, 0); // filling all values with 0
    }
    for (int i = 0; i < allUsers.size() + 1; i++) {
      for (int j = 0; j < allFood.size() + 1; j++) {
        if (i == 0 && j == 0) {
          ratingMatrix[i][j] = 0; //placing 0 at the 0,0 position of matrix
        } else if (i == 0 && j != 0) {
          ratingMatrix[i][j] = allFood.get(j - 1).getId(); //placing food id on x axis of 0th position
        } else if (j == 0 && i != 0) {
          ratingMatrix[i][j] = allUsers.get(i - 1).getId(); //placing user id on y axis of 0th position
        } else {
          Ratings rating = ratingRepository.findByUserIdAndFoodId(allUsers.get(i - 1), allFood.get(j - 1)).orElse(null);
          if (rating != null) {
            ratingMatrix[i][j] = rating.getRating(); //placing rating of user on food on their position
          }
        }
      }
    }

    print2D(ratingMatrix);
    ratingMatrix = normalizeRating(ratingMatrix);
    return ratingMatrix;
  }

  public void print2D(double mat[][]) {
    // Loop through all rows
    for (double[] row : mat)

      // converting each row as string
      // and then printing in a separate line
      System.out.println(Arrays.toString(row));
  }

  public double[][] normalizeRating(double[][] ratings) {
    for (int i = 1; i < ratings.length; i++) {
      double sum = 0;
      for (int j = 1; j < ratings[i].length; j++) {
        sum += ratings[i][j]; //adding all ratings done by user
      }
      double average = sum / (ratings[i].length - 1); //average rating of user
      for (int j = 1; j < ratings[i].length; j++) {
        if (ratings[i][j] != 0) {
          ratings[i][j] = (double) Math.round((ratings[i][j] - average) * 100) / 100; //normalizing the ratings by subtracting average rating
        }
      }
    }
    System.out.println("Normalized");
    print2D(ratings);
    return ratings;
  }

  public Map<Integer, Double> getNeighbourhood(int userId) {
    double[][] ratingMatrix = getRatingMatrix();
    Map neighbour = new HashMap();

    int userPosition = 0;
    for (int i = 0; i < ratingMatrix.length; i++) {
      if (userId == ratingMatrix[i][0]) {
        userPosition = i; //getting index of user's rating array from ratingMatrix
        break;
      }
    }
    for (int i = 1; i < ratingMatrix.length; i++) {
      if (userId == ratingMatrix[i][0]) {
        continue;
      }
      //calculate cosine distance of two users ie. cosine similarity
      double cosineDistance = computeCosineDistance(ratingMatrix[userPosition], ratingMatrix[i]);
      neighbour.put(ratingMatrix[i][0], cosineDistance);
    }
    System.out.println("neighourhood");
    System.out.println(neighbour);
    return neighbour;
  }

  public double computeCosineDistance(double[] user, double[] neighbor) {
    double dotMultiple = 0;
    for (int i = 1; i < user.length; i++) {
      dotMultiple += user[i] * neighbor[i]; //calcuating vector multiplication of two vectors
    }
    double userModule = computeModule(user); //calculating module of vector
    double neighborModule = computeModule(neighbor);
    if (userModule != 0 && neighborModule != 0) {
      return dotMultiple / (userModule * neighborModule); //calculating cosine distance by applying formula
    }
    return 0;
  }

  public double computeModule(double[] arr) {
    double sum = 0;
    for (int i = 1; i < arr.length; i++) {
      sum += Math.pow(arr[i], 2);
    }
    return Math.sqrt(sum);
  }


  public HashMap predictRating(int userId) {
    Map neighbourhood = getNeighbourhood(userId);
    LinkedHashMap orderedNeighbourhood = new LinkedHashMap<>();
    LinkedHashMap<Integer, Double> predictedRatings = new LinkedHashMap<>();
    Set<Integer> foodId = new HashSet<>();
    //sorting neighbour into ascending order according to similarity
    for (int i = 1; neighbourhood.size() >= 1; i++) {
      final double[] maxValue = {-999};
      final double[] maxKey = new double[1];
      neighbourhood.keySet().forEach(item -> {
        if (maxValue[0] < (double) neighbourhood.get(item)) {
          maxKey[0] = (double) item;
          maxValue[0] = (double) neighbourhood.get(item);
        }
      });
      orderedNeighbourhood.put(maxKey[0], neighbourhood.get(maxKey[0]));
      neighbourhood.remove(maxKey[0]);
    }

    //for each user get the food that s/he has rated until 5 food item is got
    orderedNeighbourhood.keySet().forEach(item -> {
      if (foodId.size() < 5) {
        Optional<Users> user = userRepository.findById((int) Math.round((double) item));
        if (user.isPresent()) {
          List<Ratings> ratings = ratingRepository.findByUserId(user.get());
          ratings.forEach(item1 -> {
            foodId.add(item1.getFoodId().getId());
          });
        }
      }
    });

    //compute rating prediction for the food items
    foodId.forEach(item -> {
      double predictedRating = predictCalculation(orderedNeighbourhood, item);
      predictedRatings.put(item, predictedRating);
    });
    return predictedRatings;
  }


  public double predictCalculation(Map neighbourhood, int foodId) {
    final double[] similarityTotal = {0};
    final double[] weightedRating = {0};

    neighbourhood.values().forEach(item->{
      similarityTotal[0] += (double) item;
    });

    neighbourhood.keySet().forEach(item -> {
      Optional<Users> user = userRepository.findById((int) Math.round((double) item));
      Optional<Foods> food = foodRepository.findById(foodId);
      if (user.isPresent() && food.isPresent()) {
        Optional<Ratings> rating = ratingRepository.findByUserIdAndFoodId(user.get(), food.get());
        if (rating.isPresent()) {
          weightedRating[0] = weightedRating[0] + (rating.get().getRating() * (double) neighbourhood.get(item));
        }
      }
    });
    return weightedRating[0] / similarityTotal[0];
  }

  @Autowired
  UserService userService;

  @Override
  public HashMap recommendedFood() throws Exception {
    return predictRating(userService.findCurrentUser().getId());
  }
}
