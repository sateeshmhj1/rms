package com.vedas.restaurant.service.impl;

import com.vedas.basic.response.MyException;
import com.vedas.restaurant.dao.CartRepository;
import com.vedas.restaurant.dao.FoodRepository;
import com.vedas.restaurant.database.Carts;
import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.dto.request.CartRequestDto;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import com.vedas.restaurant.mapper.CartMapper;
import com.vedas.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vedas.restaurant.service.CartService;

import java.util.List;

@Service
public class CartServiceImpl implements CartService {

  @Autowired
  private CartRepository cartRepository;

  @Autowired
  private CartMapper cartMapper;

  @Autowired
  private UserService userService;

  @Autowired
  private FoodRepository foodRepository;

  @Override
  public CartResponseDto addToCart(CartRequestDto dto) throws Exception {
    Carts cart = new Carts();
    cartMapper.toCart(cart, dto);
    Foods food = foodRepository.findById(dto.getFoodId()).orElseThrow(() -> {
      return new MyException(ErrorCode.F001, "Food id no found");
    });
    cart.setFoodId(food);
    cart.setUserId(userService.findCurrentUser().getUserDetails());
    cart.setOrderTime(System.currentTimeMillis());
    cartRepository.save(cart);
    return cartMapper.toCartResponseDto(cart);
  }

  @Override
  public List<CartResponseDto> getCarts() throws Exception {
    List<Carts> cartList = cartRepository.findByOrderIdIsNullAndUserId(userService.findCurrentUser().getUserDetails());
    return cartMapper.toCartResponseDto(cartList);
  }

  @Override
  public CartResponseDto removeFromCart(Integer id) throws Exception {
    Carts cart = cartRepository.findByIdAndOrderIdIsNullAndUserId(id, userService.findCurrentUser().getUserDetails()).orElseThrow(() -> {
      return new MyException(ErrorCode.C001, "Cart not found");
    });
    cartRepository.delete(cart);
    return cartMapper.toCartResponseDto(cart);
  }

  @Override
  public void removeAll() throws Exception {
    List<Carts> cartList = cartRepository.findByOrderIdIsNullAndUserId(userService.findCurrentUser().getUserDetails());
    cartRepository.deleteAll(cartList);
  }

  @Override
  public CartResponseDto changeStatus(Integer id, OrderedFoodStatus status) throws Exception {
    Carts cart = cartRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.CA01, "Cart not found");
    });
    if(cart.getStatus() == OrderedFoodStatus.CANCELED || cart.getStatus() == OrderedFoodStatus.DELIVERED){
      throw new MyException(ErrorCode.CA02, "Cart status cannot be changed");
    }
    cart.setStatus(status);
    cart.setUpdatedDate(System.currentTimeMillis());
    cartRepository.save(cart);
    return cartMapper.toCartResponseDto(cart);
  }

}
