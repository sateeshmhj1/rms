package com.vedas.restaurant.service.impl;

import com.vedas.basic.response.MyException;
import com.vedas.restaurant.dao.CartRepository;
import com.vedas.restaurant.dao.OrderRepository;
import com.vedas.restaurant.dao.UserOrderedFoodRepository;
import com.vedas.restaurant.dao.projection.IDs;
import com.vedas.restaurant.database.Carts;
import com.vedas.restaurant.database.Orders;
import com.vedas.restaurant.database.UserOrderedFood;
import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.dto.response.OrderResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.OrderStatus;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import com.vedas.restaurant.mapper.CartMapper;
import com.vedas.restaurant.service.OrderService;
import com.vedas.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
  @Autowired
  OrderRepository orderRepository;

  @Autowired
  UserService userService;

  @Autowired
  CartRepository cartRepository;

  @Autowired
  CartMapper cartMapper;

  @Autowired
  UserOrderedFoodRepository userOrderedFoodRepository;

  @Override
  public OrderResponseDto placeOrder(Integer tableNumber) throws Exception {
    Users user = userService.findCurrentUser();
    Double totalAmount = 0.0;
    List<Carts> cartList = cartRepository.findByOrderIdIsNullAndUserId(userService.findCurrentUser().getUserDetails());
    if(cartList.isEmpty()){
      throw new MyException(ErrorCode.C001, "Cartlist is empty");
    }
    Orders order = new Orders();
    orderRepository.save(order);

    for (Carts cart : cartList) {
      if (cart.getFoodId().getFavouritePrice() != null && cart.getFoodId().getFavouritePrice() != 0) {
        totalAmount += cart.getQuantity() * cart.getFoodId().getFavouritePrice();
        cart.setPrice(cart.getFoodId().getFavouritePrice());
      } else if(cart.getFoodId().getDiscount() != null){
        totalAmount += cart.getQuantity() * (cart.getFoodId().getPrice() - cart.getFoodId().getDiscount());
        cart.setPrice(cart.getFoodId().getPrice() - cart.getFoodId().getDiscount());
      }else {
        totalAmount += cart.getQuantity() * cart.getFoodId().getPrice();
        cart.setPrice(cart.getFoodId().getPrice());
      }
      cart.setOrderId(order);
      cart.setStatus(OrderedFoodStatus.PLACED);

      UserOrderedFood userOrderedFood =  userOrderedFoodRepository.findByUserIdAndFoodId(user, cart.getFoodId()).orElse(new UserOrderedFood());
      userOrderedFood.setOrderNumber(userOrderedFood.getOrderNumber() + cart.getQuantity());
      userOrderedFood.setFoodId(cart.getFoodId());
      userOrderedFood.setUserId(user);
      userOrderedFoodRepository.save(userOrderedFood);

    }
    order.setCartList(cartList);
    order.setTableNo(tableNumber);
    order.setOrderStatus(OrderStatus.PLACED);
    order.setOrderDate(System.currentTimeMillis());
    order.setUpdateDate(System.currentTimeMillis());
    order.setTotalAmount(totalAmount);
    order.setUserId(user.getUserDetails());
    orderRepository.save(order);
    return cartMapper.toOrderResponseDto(order);
  }

  @Override
  public Page<OrderResponseDto> getOrders(Pageable pg) throws Exception {
    Users user = userService.findCurrentUser();
    Page<Orders> orders = orderRepository.findByUserId(user.getUserDetails(), pg);
    List<OrderResponseDto> orderResponseList = cartMapper.toOrderResponseDto(orders.getContent());
    return new PageImpl<>(orderResponseList, orders.getPageable(), orders.getTotalElements());
  }

  @Override
  public Page<IDs> getAllOrders(String status, Pageable pg) throws Exception {
    Page<IDs> orders = null;
    if (status != "ALL") {
      orders = orderRepository.findAllByOrderStatus(Enum.valueOf(OrderStatus.class, status), pg);
    } else {
      orders = orderRepository.findBy(pg);
    }
    return orders;
  }

  @Override
  public OrderResponseDto getById(int id) throws Exception {
    Orders order = orderRepository.findById(id).orElseThrow(()->{
      return new MyException(ErrorCode.O001, "Order not found");
    });
    return cartMapper.toOrderResponseDto(order);
  }

  @Override
  public List<CartResponseDto> getCartFoodByStatus(OrderedFoodStatus status) throws Exception {
    List<Carts> carts = cartRepository.findByOrderIdIsNotNullAndStatus(status);
    return cartMapper.toCartResponseDto(carts);
  }

  @Override
  public void changeOrderStatus(Integer id, OrderStatus status) throws Exception {
    Orders order = orderRepository.findById(id).orElseThrow(()->{
      return new MyException(ErrorCode.O001, "Order not found");
    });
    order.setOrderStatus(status);
    orderRepository.save(order);
  }
}
