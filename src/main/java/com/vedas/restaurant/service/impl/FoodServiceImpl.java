package com.vedas.restaurant.service.impl;

import com.vedas.basic.response.MyException;
import com.vedas.restaurant.dao.FoodRepository;
import com.vedas.restaurant.dao.MenuCategoryRepository;
import com.vedas.restaurant.dao.RatingRepository;
import com.vedas.restaurant.dao.UserOrderedFoodRepository;
import com.vedas.restaurant.database.*;
import com.vedas.restaurant.dto.request.FavouriteFoodRequestDto;
import com.vedas.restaurant.dto.request.FoodRequestDto;
import com.vedas.restaurant.dto.response.FoodResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.mapper.FoodMapper;
import com.vedas.restaurant.service.FoodService;
import com.vedas.restaurant.service.UserService;
import com.vedas.restaurant.util.ImageUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FoodServiceImpl implements FoodService {

  @Autowired
  private FoodMapper foodMapper;

  @Autowired
  private MenuCategoryRepository menuCategoryRepository;

  @Autowired
  private ImageUploadUtil imageUploadUtil;

  @Autowired
  private FoodRepository foodRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private RatingRepository ratingRepository;

  @Autowired
  private UserOrderedFoodRepository userOrderedFoodRepository;

  @Override
  public void addFood(FoodRequestDto dto) throws Exception {
    if (dto.getFoodImage() == null) {
      throw new MyException(ErrorCode.V001, "Food Image cannot be null");
    }
    MenuCategory category = menuCategoryRepository.findById(dto.getCategoryId()).orElseThrow(() -> {
      return new MyException(ErrorCode.C001);
    });
    Foods food = foodMapper.toFoods(dto);
    food.setCategoryId(category);
    food.setCreatedDate(System.currentTimeMillis());
    food.setStatus(Boolean.FALSE);
    food.setPhotoURL(imageUploadUtil.imageUpload(dto.getFoodImage(), "food"));
    foodRepository.save(food);
  }

  @Override
  public Page<FoodResponseDto> getFoods(Pageable pg) throws Exception {
    Page<Foods> foods = foodRepository.findAll(pg);
    return new PageImpl<>(foodMapper.toFoodResponseDto(foods.getContent()), foods.getPageable(), foods.getTotalElements());
  }

  @Override
  public void changeFoodStatus(Integer id, Boolean status) throws Exception {
    Foods food = foodRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.F001);
    });
    food.setStatus(status);
    foodRepository.save(food);
  }

  @Override
  public void updateFood(Integer id, FoodRequestDto dto) throws Exception {
    Foods oldFood = foodRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.F001);
    });
    Foods food = foodMapper.toFoods(dto);
    food.setId(id);
    MenuCategory category = menuCategoryRepository.findById(dto.getCategoryId()).orElseThrow(() -> {
      return new MyException(ErrorCode.C001);
    });
    if (dto.getFoodImage() == null) {
      food.setPhotoURL(oldFood.getPhotoURL());
    } else {
      food.setPhotoURL(imageUploadUtil.imageUpload(dto.getFoodImage(), "food"));
    }
    food.setStatus(Boolean.TRUE);
    food.setCategoryId(oldFood.getCategoryId());
    food.setCreatedDate(oldFood.getCreatedDate());
    foodRepository.save(food);
  }

  @Override
  public FoodResponseDto favouriteFood(Integer id, FavouriteFoodRequestDto dto) throws Exception {
    Foods food = foodRepository.findById(id).orElseThrow(() -> {
      return new MyException(ErrorCode.F001);
    });
    food.setFavourite(dto.getFavourite());
    if (dto.getFavouritePrice() != null) {
      food.setFavouritePrice(dto.getFavouritePrice());
    }
    if (dto.getFavouriteDescription() != null) {
      food.setFavouriteDescription(dto.getFavouriteDescription());
    }
    Foods oldFood = foodRepository.findByFavourite(dto.getFavourite()).orElse(null);
    if (oldFood != null) {
      oldFood.setFavourite(0);
      oldFood.setFavouritePrice(0.0);
      foodRepository.save(oldFood);
    }
    foodRepository.save(food);
    return foodMapper.toFoodResponseDto(food);
  }

  @Override
  public List<FoodResponseDto> getFavouriteFood() throws Exception {
    List<Foods> foodList = foodRepository.findByFavouriteNot(0);
    return foodMapper.toFoodResponseDto(foodList);
  }

  @Override
  public void rateFood(int foodId, int rating) throws Exception {
    Foods food = foodRepository.findById(foodId).orElseThrow(()->{
      return new MyException(ErrorCode.F001);
    });
    Ratings ratings = new Ratings();
    ratings.setUserId(userService.findCurrentUser());
    ratings.setFoodId(food);
    ratings.setRating(rating);
    ratingRepository.save(ratings);
  }

  @Override
  public List<FoodResponseDto> getFrequentFood() throws Exception {
    Users user = userService.findCurrentUser();
    List<UserOrderedFood> userOrderedFoods = userOrderedFoodRepository.findTop3ByUserIdOrderByOrderNumberDesc(user);
    List<FoodResponseDto> foodResponseDtos = new ArrayList<>();
    for(UserOrderedFood userOrderedFood : userOrderedFoods){
      foodResponseDtos.add(foodMapper.toFoodResponseDto(userOrderedFood.getFoodId()));
    }
    return foodResponseDtos;
  }

}
