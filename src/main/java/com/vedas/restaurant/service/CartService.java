package com.vedas.restaurant.service;

import com.vedas.restaurant.dto.request.CartRequestDto;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartService {

  public CartResponseDto addToCart(CartRequestDto dto) throws Exception;

  public List<CartResponseDto> getCarts() throws Exception;

  public CartResponseDto removeFromCart(Integer id) throws Exception;

  public void removeAll() throws Exception;

  CartResponseDto changeStatus(Integer id, OrderedFoodStatus status) throws Exception;
}
