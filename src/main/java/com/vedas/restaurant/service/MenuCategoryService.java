package com.vedas.restaurant.service;

import com.vedas.restaurant.dto.request.MenuCategoryRequestDto;
import com.vedas.restaurant.dto.response.MenuCategoryResponseDto;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuCategoryService {

  public void addCategory(MenuCategoryRequestDto dto) throws Exception;

  public List<MenuCategoryResponseDto> getAllCategory() throws Exception;

  public List<MenuCategoryResponseDto> getCategory() throws Exception;

  public void updteCategory(int id, MenuCategoryRequestDto dto) throws Exception;

  public void deleteCategory(int id) throws Exception;

  public void changeStatus(int id, Boolean status) throws Exception;

}
