package com.vedas.restaurant.service;

import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.dto.*;
import com.vedas.restaurant.dto.response.RatingResponseDto;
import com.vedas.restaurant.dto.response.UserIdResponseDto;
import com.vedas.restaurant.dto.response.UserResponseDto;
import com.vedas.restaurant.enums.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserService {

  Users findCurrentUser() throws Exception;

  Page<UserIdResponseDto> findByUserRole(UserRole role, Pageable pg) throws Exception;

  public Users createUser(UserCreateDto dto) throws Exception;

  public String createUser(SocialUserDto dto) throws Exception;

  public void updateUser(UserCreateDto dto, Integer userId) throws Exception;

  public Users findByUsername(String username) throws Exception;

  public Users verifyUser(VerificationDto dto) throws Exception;

  public void resetPassword(ResetPasswordDto dto) throws Exception;

  public void forgotPassword(ForgetDto dto) throws Exception;

  public void sendVerificationCode(String phoneNumber) throws Exception;

  Page<UserResponseDto> getStaffs(Pageable pg) throws Exception;

  void createStaff(StaffCreateDto dto) throws Exception;

  void updateStaff(int id, StaffCreateDto dto) throws Exception;

  List<RatingResponseDto> getRatings() throws Exception;
}
