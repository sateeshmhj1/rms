package com.vedas.restaurant.controller;

import com.vedas.basic.response.ServiceResponse;
import com.vedas.restaurant.dao.projection.IDs;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.dto.response.OrderResponseDto;
import com.vedas.restaurant.enums.OrderStatus;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import com.vedas.restaurant.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("api/order")
@Secured("ROLE_CHEF")
public class OrderController {

  @Autowired
  OrderService orderService;

  @PostMapping("{tableNumber}")
  @Secured("ROLE_USER")
  public ServiceResponse<OrderResponseDto> placeOrder(@PathVariable Integer tableNumber) throws Exception {
    ServiceResponse<OrderResponseDto> response = new ServiceResponse<>();
    response.setData(orderService.placeOrder(tableNumber));
    return response;
  }

  @GetMapping
  @Secured("ROLE_USER")
  public ServiceResponse<Page<OrderResponseDto>> getOrders(Pageable pg) throws Exception {
    ServiceResponse<Page<OrderResponseDto>> response = new ServiceResponse<>();
    response.setData(orderService.getOrders(pg));
    return response;
  }

  @GetMapping("status/{status}")
  @Secured({"ROLE_ADMIN", "ROLE_WAITER", "ROLE_CHEF", "ROLE_CASHIER"})
  public ServiceResponse<Page<IDs>> getAllOrders(@PathVariable String status, Pageable pg) throws Exception {
    ServiceResponse<Page<IDs>> response = new ServiceResponse<>();
    response.setData(orderService.getAllOrders(status, pg));
    return response;
  }

  @PutMapping("status/{id}/{status}")
  @Secured({"ROLE_ADMIN", "ROLE_WAITER", "ROLE_CHEF", "ROLE_CASHIER"})
  public ServiceResponse changeOrderStatus(@PathVariable Integer id, @PathVariable OrderStatus status) throws Exception {
    ServiceResponse response = new ServiceResponse();
    orderService.changeOrderStatus(id, status);
    return response;
  }

  @GetMapping("{id}")
  @Secured({"ROLE_ADMIN", "ROLE_WAITER", "ROLE_CHEF", "ROLE_CASHIER"})
  public ServiceResponse<OrderResponseDto> getOrderById(@PathVariable int id) throws Exception {
    ServiceResponse<OrderResponseDto> response = new ServiceResponse<>();
    response.setData(orderService.getById(id));
    return response;
  }

  @GetMapping("cart-food/{status}")
  @Secured({"ROLE_ADMIN", "ROLE_WAITER", "ROLE_CHEF", "ROLE_CASHIER"})
  public ServiceResponse<List<CartResponseDto>> getCartFoodByStatus(@PathVariable OrderedFoodStatus status) throws Exception{
    ServiceResponse<List<CartResponseDto>> response = new ServiceResponse<>();
    response.setData(orderService.getCartFoodByStatus(status));
    return response;
  }

}
