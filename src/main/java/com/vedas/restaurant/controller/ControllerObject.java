package com.vedas.restaurant.controller;

import com.vedas.restaurant.dao.UserRepository;
import com.vedas.restaurant.mapper.UserMapper;
import com.vedas.restaurant.service.EmailService;
import com.vedas.restaurant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public class ControllerObject {

    @Autowired
    public UserService userService;

    @Autowired
    public UserRepository userDao;

    @Autowired
    public EmailService emailService;

    
    @Autowired
    public PasswordEncoder passwordEncoder;
    
    @Autowired
    public UserMapper userMapper;
}
