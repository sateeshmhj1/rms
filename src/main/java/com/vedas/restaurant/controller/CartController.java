package com.vedas.restaurant.controller;

import com.vedas.basic.response.MyException;
import com.vedas.basic.response.ServiceResponse;
import com.vedas.restaurant.dto.request.CartRequestDto;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import com.vedas.restaurant.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/cart")
public class CartController {

  @Autowired
  private CartService cartService;

  @PostMapping
  @Secured("ROLE_USER")
  public ServiceResponse<CartResponseDto> addToCart(@RequestBody CartRequestDto dto, @Valid BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse<CartResponseDto> response = new ServiceResponse<>();
    response.setData(cartService.addToCart(dto));
    return response;
  }

  @GetMapping
  @Secured("ROLE_USER")
  public ServiceResponse<List<CartResponseDto>> getCarts() throws Exception {
    ServiceResponse<List<CartResponseDto>> response = new ServiceResponse<>();
    response.setData(cartService.getCarts());
    return response;
  }

  @DeleteMapping("{id}")
  @Secured("ROLE_USER")
  public ServiceResponse<CartResponseDto> removeFromCart(@PathVariable Integer id) throws Exception {
    ServiceResponse<CartResponseDto> response = new ServiceResponse<>();
    response.setData(cartService.removeFromCart(id));
    return response;
  }

  @DeleteMapping("all")
  @Secured("ROLE_USER")
  public ServiceResponse removeAll() throws Exception {
    ServiceResponse response = new ServiceResponse();
    cartService.removeAll();
    return response;
  }

  @PutMapping("status/{id}/{status}")
  @Secured("ROLE_ADMIN")
  public ServiceResponse<CartResponseDto> changeStatus(@PathVariable Integer id, @PathVariable OrderedFoodStatus status) throws Exception {
    ServiceResponse<CartResponseDto> response = new ServiceResponse();
    response.setData(cartService.changeStatus(id, status));
    return response;
  }

}
