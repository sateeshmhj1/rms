package com.vedas.restaurant.controller;

import com.vedas.basic.response.MyException;
import com.vedas.basic.response.ServiceResponse;
import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.dto.StaffCreateDto;
import com.vedas.restaurant.dto.UserCreateDto;
import com.vedas.restaurant.dto.response.RatingResponseDto;
import com.vedas.restaurant.dto.response.UserResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.MessageCode;
import com.vedas.restaurant.service.PearsonCorrelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/user")
public class UserController extends ControllerObject {

  @Autowired
  PearsonCorrelationService pearsonCorrelationService;

  @GetMapping("test")
  public ServiceResponse test() throws Exception {
    ServiceResponse response = new ServiceResponse();
    pearsonCorrelationService.recommendedFood();
    return response;
  }

  @GetMapping
  @Secured({"ROLE_USER", "ROLE_ADMIN", "ROLE_WAITER", "ROLE_CHEF", "ROLE_CASHIER"})
  public ServiceResponse<UserResponseDto> getUser() throws Exception {
    ServiceResponse<UserResponseDto> response = new ServiceResponse();
    Users user = userService.findCurrentUser();
    response.setData(userMapper.toUserResponseDto(user));
    return response;
  }

  @GetMapping("all")
  public ServiceResponse<Page<Users>> getUsers(Pageable pg) {
    ServiceResponse<Page<Users>> response = new ServiceResponse();
    Page<Users> all = userDao.findAll(pg);
    response.setData(all);
    return response;
  }

  @GetMapping("staff")
  @Secured(("ROLE_ADMIN"))
  public ServiceResponse<Page<UserResponseDto>> getStaff(Pageable pg) throws Exception {
    ServiceResponse<Page<UserResponseDto>> response = new ServiceResponse<>();
    response.setData(userService.getStaffs(pg));
    return response;
  }

  @PostMapping("staff")
  public ServiceResponse addStaff(@RequestBody @Valid StaffCreateDto dto, BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse();
    userService.createStaff(dto);
    return response;
  }

  @PutMapping("staff/{id}")
  public ServiceResponse updateStaff(@PathVariable int id, @RequestBody @Valid StaffCreateDto dto, BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse();
    userService.updateStaff(id, dto);
    return response;

  }

  @PutMapping
  public ServiceResponse updateUser(@RequestBody @Valid UserCreateDto dto, Integer userId, BindingResult result)
      throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse(MessageCode.M002);
    userService.updateUser(dto, userId);
    return response;
  }

  @GetMapping("ratings")
  @Secured("ROLE_USER")
  public ServiceResponse<List<RatingResponseDto>> getRatings() throws Exception {
    ServiceResponse<List<RatingResponseDto>> response = new ServiceResponse<>();
    response.setData(userService.getRatings());
    return response;
  }

}
