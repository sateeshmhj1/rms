//package com.vedas.restaurant.controller;
//
//import com.vedas.basic.response.MyException;
//import com.vedas.basic.response.ServiceResponse;
//import com.vedas.basic.utils.TokenUtil;
//import com.vedas.restaurant.dto.SocialUserDto;
//import com.vedas.restaurant.enums.ErrorCode;
//import com.vedas.restaurant.enums.MessageCode;
//import com.vedas.restaurant.enums.Providers;
//import com.vedas.restaurant.service.UserService;
//import com.vedas.restaurant.social.AccessTokenData;
//import com.vedas.restaurant.social.FacebookData;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.client.HttpStatusCodeException;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@RestController
//@RequestMapping("api/auth/social")
//public class SocialAuthController {
//
//    private RestTemplate restTemplate = new RestTemplate();
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    private TokenUtil tokenUtil;
//
//    @Autowired
//    private UserDetailsService userDetailsService;
//
//    @GetMapping("{provider}")
//    public ServiceResponse<String> socialAuth(@RequestHeader String accessToken, @PathVariable String provider) throws Exception {
//        SocialUserDto dto = new SocialUserDto();
//        if (provider.equals("facebook")) {
//            Map<?, ?> fbUser = getFacebookUser(accessToken);
//            dto.setFullName((String) fbUser.get("name"));
//            dto.setProviderId((String) fbUser.get("id"));
//            dto.setPhotoURL((String) ((Map) ((Map) fbUser.get("picture")).get("data")).get("url"));
//            dto.setEmail((String) fbUser.get("email"));
//            dto.setProvider(Providers.FACEBOOK);
//        } else {
//
//        }
//        String username = userService.createUser(dto);
//        final UserDetails userDetails = (UserDetails) userDetailsService.loadUserByUsername(username);
//        final String token = tokenUtil.generateToken(userDetails);
//        ServiceResponse<String> response = new ServiceResponse<>(MessageCode.M001);
//        response.setData(token);
//        return response;
//    }
//
//    Map<?, ?> getFacebookUser(String accessToken) throws Exception {
//        String appAccessToken = null;
//        try {
//            appAccessToken = getAppAccessToken();
//        } catch (RuntimeException e) {
//        }
//
//        //verifying access token 
//        AccessTokenData accessTokenData = inspectAccessTokenData(accessToken, appAccessToken);
//        if (!accessTokenData.isIs_valid() || accessTokenData.getApp_id() != Long.valueOf(appId)) {
//            throw new MyException(ErrorCode.T001);
//        }
//        return getUserDetailsFromAccessToken(accessToken);
//    }
//
//    @Value(value = "${facebook.app.id}")
//    String appId;
//
//    @Value(value = "${facebook.app.secret}")
//    String appSecret;
//
//    private String getAppAccessToken() throws Exception {
//        Map<String, String> urlParams = new HashMap<>();
//        urlParams.put("client_id", appId);
//        urlParams.put("client_secret", appSecret);
//        try {
//            String json = restTemplate.getForObject(
//                    "https://graph.facebook.com/oauth/access_token?client_id={client_id}&client_secret={client_secret"
//                    + "}&grant_type=client_credentials",
//                    String.class, urlParams);
//            return new JSONObject(json).getString("access_token");
//        } catch (HttpStatusCodeException exception) {
//            throw new MyException(ErrorCode.T001);
//        }
//    }
//
//    private AccessTokenData inspectAccessTokenData(String token, String appAccessToken) throws MyException {
//        Map<String, String> urlparams = new HashMap<>();
//        urlparams.put("input_token", token);
//        urlparams.put("access_token", appAccessToken);
//        try {
//            return restTemplate.getForObject(
//                    "https://graph.facebook.com/debug_token?input_token={input_token}&access_token={access_token}",
//                    FacebookData.class, urlparams).getData();
//        } catch (HttpStatusCodeException exception) {
//            throw new MyException(ErrorCode.T001);
//        }
//    }
//
//    private Map<?, ?> getUserDetailsFromAccessToken(String token) throws MyException {
//
//        Map<String, String> urlparams = new HashMap<>();
//        urlparams.put("access_token", token);
//        urlparams.put("fields", "id,name,email,picture");
//
//        try {
//            return restTemplate.getForObject(
//                    "https://graph.facebook.com/v2.9/me/?access_token={access_token}&fields={fields}", Map.class,
//                    urlparams);
//        } catch (HttpStatusCodeException exception) {
//            throw new MyException(ErrorCode.T001);
//        }
//    }
//
//}
