package com.vedas.restaurant.controller;

import com.vedas.basic.response.MyException;
import com.vedas.basic.response.ServiceResponse;
import com.vedas.restaurant.dto.request.MenuCategoryRequestDto;
import com.vedas.restaurant.dto.response.MenuCategoryResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.MessageCode;
import com.vedas.restaurant.service.MenuCategoryService;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/category")
public class CategoryController {

  @Autowired
  MenuCategoryService categoryService;

  @PostMapping
  public ServiceResponse addCategory(@RequestBody MenuCategoryRequestDto dto, @Valid BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
              .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse(MessageCode.C001);
    categoryService.addCategory(dto);
    return response;
  }

  @GetMapping("all")
  @Secured("ROLE_ADMIN")
  public ServiceResponse<List<MenuCategoryResponseDto>> getAllCategory() throws Exception {
    ServiceResponse<List<MenuCategoryResponseDto>> response = new ServiceResponse<>();
    response.setData(categoryService.getAllCategory());
    return response;
  }

  @GetMapping
  public ServiceResponse<List<MenuCategoryResponseDto>> getCategory() throws Exception {
    ServiceResponse<List<MenuCategoryResponseDto>> response = new ServiceResponse<>();
    response.setData(categoryService.getCategory());
    return response;
  }

  @PutMapping("{id}")
  public ServiceResponse updateCategory(@PathVariable int id, @RequestBody MenuCategoryRequestDto dto, @Valid BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
              .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse(MessageCode.C002);
    categoryService.updteCategory(id, dto);
    return response;
  }

  @PutMapping("{id}/{status}")
  public ServiceResponse changeCategoryStatus(@PathVariable int id, @PathVariable Boolean status) throws Exception {
    ServiceResponse response = new ServiceResponse(MessageCode.C004);
    categoryService.changeStatus(id, status);
    return  response;
  }

  @DeleteMapping("{id}")
  public ServiceResponse deleteCategory(@PathVariable int id) throws Exception {
    ServiceResponse response = new ServiceResponse(MessageCode.C003);
    categoryService.deleteCategory(id);
    return response;
  }
}
