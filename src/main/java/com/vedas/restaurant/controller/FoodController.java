package com.vedas.restaurant.controller;

import com.vedas.basic.response.MyException;
import com.vedas.basic.response.ServiceResponse;
import com.vedas.restaurant.dto.request.FavouriteFoodRequestDto;
import com.vedas.restaurant.dto.request.FoodRequestDto;
import com.vedas.restaurant.dto.response.FoodResponseDto;
import com.vedas.restaurant.enums.ErrorCode;
import com.vedas.restaurant.enums.MessageCode;
import com.vedas.restaurant.service.FoodService;
import com.vedas.restaurant.service.PearsonCorrelationService;
import com.vedas.restaurant.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/food")
public class FoodController {

  @Autowired
  private FoodService foodService;

  @Autowired
  RecommendationService recommendationService;

  @PostMapping
  public ServiceResponse addFood(@RequestBody @Valid FoodRequestDto dto, BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse<>(MessageCode.F001);
    foodService.addFood(dto);
    return response;
  }

  @GetMapping("all")
  @Secured("ROLE_ADMIN")
  public ServiceResponse<Page<FoodResponseDto>> getFood(Pageable pg) throws Exception {
    ServiceResponse<Page<FoodResponseDto>> response = new ServiceResponse<>();
    response.setData(foodService.getFoods(pg));
    return response;
  }

  @PutMapping("{id}")
  @Secured("ROLE_ADMIN")
  public ServiceResponse updateFood(@PathVariable Integer id, @RequestParam FoodRequestDto dto, @Valid BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse response = new ServiceResponse();
    foodService.updateFood(id, dto);
    return response;
  }

  @PutMapping("{id}/{status}")
  @Secured("ROLE_ADMIN")
  public ServiceResponse changeFoodStatus(@PathVariable Integer id, @PathVariable Boolean status) throws Exception {
    ServiceResponse response = new ServiceResponse();
    foodService.changeFoodStatus(id, status);
    return response;
  }

  @PutMapping("favourite/{id}")
  @Secured("ROLE_ADMIN")
  public ServiceResponse<FoodResponseDto> favouriteFood(@PathVariable Integer id, @RequestBody FavouriteFoodRequestDto dto, @Valid BindingResult result) throws Exception {
    if (result.hasErrors()) {
      Map<String, String> errors = result.getFieldErrors().stream()
          .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
      throw new MyException(ErrorCode.V001, errors);
    }
    ServiceResponse<FoodResponseDto> response = new ServiceResponse<>();
    response.setData(foodService.favouriteFood(id, dto));
    return response;
  }

  @GetMapping("favourite")
  public ServiceResponse<List<FoodResponseDto>> getFavouriteFood() throws Exception {
    ServiceResponse<List<FoodResponseDto>> response = new ServiceResponse<>();
    response.setData(foodService.getFavouriteFood());
    return response;
  }

  @Autowired
  PearsonCorrelationService pearsonCorrelationService;
  @GetMapping("getRecommendedFood")
  @Secured("ROLE_USER")
  public ServiceResponse<HashMap> getRecommendedFood() throws Exception {
    ServiceResponse<HashMap> response = new ServiceResponse<>();
    response.setData(pearsonCorrelationService.recommendedFood());
    return response;
  }

  @PostMapping("rate/{foodId}/{rating}")
  @Secured("ROLE_USER")
  public ServiceResponse rateFood(@PathVariable int foodId, @PathVariable int rating) throws Exception{
    ServiceResponse response = new ServiceResponse();
    foodService.rateFood(foodId, rating);
    return response;
  }

  @GetMapping("frequent")
  @Secured("ROLE_USER")
  public ServiceResponse<List<FoodResponseDto>> getFrequentFood() throws Exception{
    ServiceResponse<List<FoodResponseDto>> response = new ServiceResponse<>();
    response.setData(foodService.getFrequentFood());
    return response;
  }

}
