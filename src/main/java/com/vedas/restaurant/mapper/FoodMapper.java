package com.vedas.restaurant.mapper;

import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.dto.request.FoodRequestDto;
import com.vedas.restaurant.dto.response.FoodResponseDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class FoodMapper {

  @Mappings({
    @Mapping(target = "categoryId", ignore = true)
  })
  public abstract Foods toFoods(FoodRequestDto dto);

  @Mappings({
    @Mapping(target = "categoryId", source = "categoryId.id")
  })
  public abstract FoodResponseDto toFoodResponseDto(Foods foods);

  public abstract List<FoodResponseDto> toFoodResponseDto(List<Foods> foods);

}
