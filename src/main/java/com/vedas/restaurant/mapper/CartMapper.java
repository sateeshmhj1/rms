package com.vedas.restaurant.mapper;

import com.vedas.restaurant.database.Carts;
import com.vedas.restaurant.database.Orders;
import com.vedas.restaurant.dto.request.CartRequestDto;
import com.vedas.restaurant.dto.response.CartResponseDto;
import com.vedas.restaurant.dto.response.OrderResponseDto;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class CartMapper {
  
  @Autowired
  private FoodMapper foodMapper;
  
  @Mapping(target = "foodId", ignore = true)
  public abstract void toCart(@MappingTarget Carts cart, CartRequestDto dto);
  
  @Mappings({
      @Mapping(target = "foodId", ignore = true),
      @Mapping(target = "orderId", source = "orderId.id")
  })
  public abstract CartResponseDto toCartResponseDto(Carts cart);
  
  public abstract List<CartResponseDto> toCartResponseDto(List<Carts> cart);
  
  @AfterMapping
  public void toCartResponseDto(@MappingTarget CartResponseDto dto, Carts cart) {
    dto.setFoodId(foodMapper.toFoodResponseDto(cart.getFoodId()));
  }

  public abstract OrderResponseDto toOrderResponseDto(Orders order);

  public abstract List<OrderResponseDto> toOrderResponseDto(List<Orders> order);

}
