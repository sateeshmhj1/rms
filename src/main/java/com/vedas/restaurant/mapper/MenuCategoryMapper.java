package com.vedas.restaurant.mapper;

import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.database.MenuCategory;
import com.vedas.restaurant.dto.request.MenuCategoryRequestDto;
import com.vedas.restaurant.dto.response.MenuCategoryResponseDto;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class MenuCategoryMapper {
  
  @Mapping(target = "categoryId", ignore = true)
  public abstract MenuCategory toMenuCategory(MenuCategoryRequestDto dto);
  
  @Mappings({
    @Mapping(target = "categoryId", source = "categoryId.id"),
    @Mapping(target = "foodList", ignore = true)
  })
  public abstract MenuCategoryResponseDto toMenuCategoryResponseDto(MenuCategory category);
  
  public abstract List<MenuCategoryResponseDto> toMenuCategoryResponseDtoList(List<MenuCategory> categoryList);
  
  @Autowired
  FoodMapper foodMapper;
  
  @AfterMapping
  public void toMenuCategoryResponseDto(@MappingTarget MenuCategoryResponseDto dto, MenuCategory category) {
    if (!category.getFoodList().isEmpty()) {
      List<Foods> activeFoodList = new ArrayList<>();
      activeFoodList = category.getFoodList().stream().filter(food -> (food.getStatus() == true)).collect(Collectors.toList());
      dto.setFoodList(foodMapper.toFoodResponseDto(activeFoodList));
    }
  }

  @Mapping(target = "categoryId", ignore = true)
  public abstract void toMenuCategory(@MappingTarget MenuCategory oldCategory, MenuCategoryRequestDto dto);
}
