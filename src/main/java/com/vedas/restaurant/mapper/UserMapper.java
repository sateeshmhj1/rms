package com.vedas.restaurant.mapper;

import com.vedas.restaurant.database.Ratings;
import com.vedas.restaurant.database.UserDetails;
import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.dto.StaffCreateDto;
import com.vedas.restaurant.dto.UserCreateDto;
import com.vedas.restaurant.dto.response.RatingResponseDto;
import com.vedas.restaurant.dto.response.UserIdResponseDto;
import com.vedas.restaurant.dto.response.UserResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

  Users toUser(UserCreateDto cdr);

  List<UserIdResponseDto> toUserResponseList(List<Users> page);

  List<UserResponseDto> toUserResponse(List<Users> page);

  @Mappings({
      @Mapping(target = "fullName", source = "userDetails.fullName"),
      @Mapping(target = "gender", source = "userDetails.gender"),
      @Mapping(target = "phoneNumber", source = "userDetails.phoneNumber"),
  })
  UserResponseDto toUserResponseDto(Users user);

  Users toUser(StaffCreateDto dto);

  UserDetails toUserDetails(StaffCreateDto dto);

  void toUser(@MappingTarget Users user, StaffCreateDto dto);

  void toUserDetails(@MappingTarget UserDetails userDetails, StaffCreateDto dto);

  List<RatingResponseDto> toRatingResponseDto(List<Ratings> ratings);

}
