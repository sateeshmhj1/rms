package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vedas.restaurant.enums.Providers;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
public class UserProvider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Size(max = 200)
    private String providerId;

    @Enumerated(EnumType.STRING)
    private Providers provider;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    @ManyToOne
    private Users userId;

}
