/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@Table(name = "user_details")
@NamedQueries({
  @NamedQuery(name = "UserDetails.findAll", query = "SELECT u FROM UserDetails u")})
public class UserDetails implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Column(name = "id")
  private Integer id;
  @Size(max = 200)
  @Column(name = "full_name")
  private String fullName;
  @Size(max = 100)
  @Column(name = "gender")
  private String gender;
  @Size(max = 200)
  @Column(name = "phone_number")
  private String phoneNumber;
  @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
  @OneToOne(optional = false)
  private Users users;
  @JsonIgnore
  @OneToMany(mappedBy = "userId")
  private List<Orders> orderList;
}
