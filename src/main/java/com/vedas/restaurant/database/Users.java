package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vedas.restaurant.enums.UserRole;
import com.vedas.restaurant.enums.UserStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;

    @Column(name = "last_password_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordDate;

    @Column(name = "password")
    private String password;

    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Column(name = "user_status")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @Size(max = 200)
    @Column(name = "username")
    private String username;

    @Basic(optional = false)
    @NotNull
    @Column(name = "verification_code")
    private int verificationCode;

    @Column(name = "last_login_date")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    private Date lastLoginDate;

    @Column(name = "last_password_reset_date")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    private Date lastPasswordResetDate;

    @Size(max = 500)
    @Column(name = "firebase_token")
    @JsonIgnore
    private String firebaseToken;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "users")
    @JsonIgnore
    private UserDetails userDetails;

    @OneToMany(mappedBy = "userId")
    private List<Ratings> ratings;

    private String photoURL;

}
