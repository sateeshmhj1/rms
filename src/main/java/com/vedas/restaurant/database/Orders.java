package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vedas.restaurant.enums.OrderStatus;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Orders {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private Double totalAmount;

  @OneToMany(mappedBy = "orderId")
  private List<Carts> cartList;

  private Long orderDate;

  private Long updateDate;

  @Enumerated(EnumType.STRING)
  private OrderStatus orderStatus;

  private Integer tableNo;

  @JoinColumn(name = "user_id", referencedColumnName = "id")
  @ManyToOne
  @JsonIgnore
  private UserDetails userId;

}
