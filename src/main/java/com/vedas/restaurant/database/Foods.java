package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Foods {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;

  private String photoURL;

  private Integer favourite = 0;

  private Double favouritePrice = 0.0;
  
  private String favouriteDescription = "";

  private Long createdDate;

  private Boolean status;

  private Double price;

  private Double discount;

  private Integer saleCount;

  private Integer prepareTime;

  @JoinColumn(name = "category_id", referencedColumnName = "id")
  @ManyToOne
  @JsonIgnore
  private MenuCategory categoryId;

  @OneToMany(mappedBy = "foodId")
  @JsonIgnore
  private List<Ratings> ratingsList;
}
