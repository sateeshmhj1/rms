package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vedas.restaurant.enums.CategoryType;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "menu_category")
public class MenuCategory {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String categoryName;

  @Enumerated(EnumType.STRING)
  private CategoryType categoryType;

  @JoinColumn(name = "category_id", referencedColumnName = "id")
  @ManyToOne
  @JsonIgnore
  private MenuCategory categoryId;

  @OneToMany(mappedBy = "categoryId", orphanRemoval = true)
  @JsonIgnore
  private List<MenuCategory> subCategoryList;
  
  private String photoURL;
  
  private Boolean status;

  @OneToMany(mappedBy = "categoryId")
  @JsonIgnore
  private List<Foods> foodList;

}
