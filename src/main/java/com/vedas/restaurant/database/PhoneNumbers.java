package com.vedas.restaurant.database;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Data;


@Entity
@Data
@Table(name = "phone_numbers")
public class PhoneNumbers {
    @Id
    @Size(max = 10)
    private String phoneNumber;
    private Integer verificationCode;
    private Long signupDate;
}
