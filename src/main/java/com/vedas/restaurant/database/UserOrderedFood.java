package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class UserOrderedFood {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  @JsonIgnore
  private Users userId;

  @ManyToOne
  @JoinColumn(name = "food_id", referencedColumnName = "id")
  @JsonIgnore
  private Foods foodId;

  private int orderNumber = 0;
}
