package com.vedas.restaurant.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Carts {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private Integer quantity;

  private Double price;

  private Long orderTime;

  @Enumerated(EnumType.STRING)
  private OrderedFoodStatus status;

  @JoinColumn(name = "food_id", referencedColumnName = "id")
  @ManyToOne
  @JsonIgnore
  private Foods foodId;

  @JoinColumn(name = "order_id", referencedColumnName = "id")
  @ManyToOne
  @JsonIgnore
  private Orders orderId;

  @JoinColumn(name = "user_id", referencedColumnName = "id")
  @ManyToOne
  @JsonIgnore
  private UserDetails userId;

  private Long updatedDate;

}
