package com.vedas.restaurant.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ImageUploadUtil {

  @Value("${app.images}")
  public String imageLocation;

  @PostConstruct
  public void create() {
    File file = new File(imageLocation);
    file.mkdirs();
  }

  public String imageUpload(String base64Data, String name) {
    try {
      String[] parts = base64Data.split(",");
      String imageString = parts[1];
      BufferedImage image = null;
      byte[] imageByte;
      imageByte = Base64.decodeBase64(imageString);
      ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
      image = ImageIO.read(bis);
      bis.close();
      String imageName = name + "_" + new Date().getTime() + ".png";
      File outputfile = new File(imageLocation + imageName);
      ImageIO.write(image, "png", outputfile);
      return imageName;
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return null;
  }

}
