package com.vedas.restaurant.social;

import lombok.Data;

@Data
public class AccessTokenData {

    private long app_id;
    private String application;
    private long expires_at;
    private boolean is_valid;
    private long issued_at;
    private long user_id;

    public boolean isIs_valid() {
        return is_valid;
    }

    public void setIs_valid(boolean is_valid) {
        this.is_valid = is_valid;
    }
    
}
