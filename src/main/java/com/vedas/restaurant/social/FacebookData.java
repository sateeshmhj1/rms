package com.vedas.restaurant.social;

import lombok.Data;

@Data
public class FacebookData {
    AccessTokenData data;
}
