package com.vedas.restaurant.dto;

import com.vedas.restaurant.enums.Providers;
import lombok.Data;

@Data
public class SocialUserDto {

    private String providerId;
    private Providers provider;
    private String fullName;
    private String email;
    private String photoURL;    

}
