package com.vedas.restaurant.dto;

import com.vedas.restaurant.enums.UserRole;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class StaffCreateDto {

  @NotNull
  private String fullName;
  @NotNull(message = "V004")
  @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}", message = "V005")
  private String password;
  @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "V003")
  private String email;
  @NotNull
  private UserRole userRole;
  @NotNull
  private String username;
  @NotNull
  private String gender;

}
