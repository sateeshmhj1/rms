package com.vedas.restaurant.dto.response;

import com.vedas.restaurant.enums.CategoryType;
import java.util.List;
import lombok.Data;

@Data
public class MenuCategoryResponseDto {

  private int id;

  private String categoryName;

  private CategoryType categoryType;

  private int categoryId; 

  private String photoURL;

  private Boolean status;

  private List<FoodResponseDto> foodList;
  
}
