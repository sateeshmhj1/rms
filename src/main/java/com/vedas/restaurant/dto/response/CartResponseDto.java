package com.vedas.restaurant.dto.response;

import com.vedas.restaurant.enums.OrderedFoodStatus;
import lombok.Data;

@Data
public class CartResponseDto {

  private Integer id;

  private Integer quantity;

  private Double price;

  private Long orderTime;

  private OrderedFoodStatus status;

  private Integer orderId;

  private FoodResponseDto foodId;

}
