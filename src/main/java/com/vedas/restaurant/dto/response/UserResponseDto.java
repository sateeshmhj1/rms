package com.vedas.restaurant.dto.response;

import com.vedas.restaurant.enums.UserRole;
import lombok.Data;

@Data
public class UserResponseDto {

    private Integer id;
    private String fullName;
    private String gender;
    private String phoneNumber;
    
    private String photoURL;
    private String username;
    private UserRole userRole;
    private String email;

}
