package com.vedas.restaurant.dto.response;

import com.vedas.restaurant.dto.UserCreateDto;
import lombok.Data;

@Data
public class UserIdResponseDto extends UserCreateDto {

  private Integer id;

}
