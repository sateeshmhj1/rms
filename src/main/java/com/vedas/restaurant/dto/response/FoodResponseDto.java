package com.vedas.restaurant.dto.response;

import lombok.Data;

@Data
public class FoodResponseDto {

  private Integer id;

  private String name;

  private String photoURL;

  private Long createdDate;

  private Boolean status;

  private Double price;

  private Double discount;

  private Integer saleCount;

  private Integer categoryId;

  private Integer favourite;

  private Double favouritePrice;

}
