package com.vedas.restaurant.dto.response;

import com.vedas.restaurant.enums.OrderStatus;
import lombok.Data;

import java.util.List;

@Data
public class OrderResponseDto {
  private Integer id;

  private Double totalAmount;

  private Long orderDate;

  private Long updateDate;

  private OrderStatus orderStatus;

  private List<CartResponseDto> cartList;
}
