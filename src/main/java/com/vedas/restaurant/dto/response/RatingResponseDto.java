package com.vedas.restaurant.dto.response;

import com.vedas.restaurant.database.Foods;
import lombok.Data;

@Data
public class RatingResponseDto {
  private int id;

  private Foods foodId;

  private int rating;
}
