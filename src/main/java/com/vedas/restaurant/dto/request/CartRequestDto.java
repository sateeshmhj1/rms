package com.vedas.restaurant.dto.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CartRequestDto {

  @NotNull(message = "Food id cannot be null")
  private Integer foodId;

  @NotNull(message = "Quantity cannot be null")
  private Integer quantity;

}
