package com.vedas.restaurant.dto.request;

import com.vedas.restaurant.enums.CategoryType;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class MenuCategoryRequestDto {

  @NotNull(message = "Category name cannnot be null")
  private String categoryName;

  private Integer categoryId;

  private CategoryType  categoryType;
  
  private String image;

}
