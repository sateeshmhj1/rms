package com.vedas.restaurant.dto.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class FavouriteFoodRequestDto {

  @NotNull(message = "Favourite position cannot be null")
  private Integer favourite;

  private Double favouritePrice;
  
  private String favouriteDescription;

}
