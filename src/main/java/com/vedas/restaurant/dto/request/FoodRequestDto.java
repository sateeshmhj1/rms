package com.vedas.restaurant.dto.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class FoodRequestDto {

  @NotNull(message = "Food name cannot be null")
  private String name;

  private String foodImage;

  @NotNull(message = "CategoryId cannot be null")
  private Integer categoryId;

  @NotNull(message = "Price cannot be null")
  private Double price;

  private Double discount;

  @NotNull(message = "Prepare time cannot be null")
  private Integer prepareTime;
}
