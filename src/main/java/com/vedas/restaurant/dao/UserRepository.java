package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.Users;
import com.vedas.restaurant.enums.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Integer> {

    public Optional<Users> findByUsername(String username);

    public Page<Users> findByUserRole(UserRole userRole, Pageable pg);

    public List<Users> findByUserRole(UserRole userRole);

    public Optional<Users> findByUsernameAndVerificationCode(String username, int verificationCode);

    public Optional<Users> findByUsernameOrEmail(String username, String username0);

    public Optional<Users> findByEmail(String email);

    Page<Users> findByUserRoleNot(UserRole roleUser, Pageable pg);
}
