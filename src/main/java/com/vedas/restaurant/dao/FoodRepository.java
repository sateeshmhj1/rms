package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.Foods;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FoodRepository extends JpaRepository<Foods, Integer> {

  public Optional<Foods> findByFavourite(Integer favourite);

  public List<Foods> findByFavouriteNot(Integer i);

  List<Foods> findByStatusIsTrue();
}
