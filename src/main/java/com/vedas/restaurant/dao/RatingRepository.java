package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.database.Ratings;
import com.vedas.restaurant.database.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RatingRepository extends JpaRepository<Ratings, Integer> {
  List<Ratings> findByUserId(Users currentUser);

  Optional<Ratings> findByUserIdAndFoodId(Users users, Foods foods);

  List<Ratings> findByUserIdAndRatingGreaterThan(Optional<Users> byId, int i);
}
