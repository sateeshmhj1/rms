package com.vedas.restaurant.dao;

import com.vedas.restaurant.dao.projection.IDs;
import com.vedas.restaurant.database.Orders;
import com.vedas.restaurant.database.UserDetails;
import com.vedas.restaurant.enums.OrderStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrderRepository extends JpaRepository<Orders, Integer> {
  Page<Orders> findByUserId(UserDetails userDetails, Pageable pg);

  Page<IDs> findAllByOrderStatus(OrderStatus valueOf, Pageable pg);

  Page<IDs> findBy(Pageable pg);
}
