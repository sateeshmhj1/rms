package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.Foods;
import com.vedas.restaurant.database.UserOrderedFood;
import com.vedas.restaurant.database.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserOrderedFoodRepository extends JpaRepository<UserOrderedFood, Integer> {
  Optional<UserOrderedFood> findByUserIdAndFoodId(Users user, Foods foodId);

  List<UserOrderedFood> findTop3ByUserIdOrderByOrderNumberDesc(Users user);
}
