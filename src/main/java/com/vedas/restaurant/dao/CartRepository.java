package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.Carts;
import com.vedas.restaurant.database.UserDetails;
import com.vedas.restaurant.enums.OrderedFoodStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CartRepository extends JpaRepository<Carts, Integer> {

  public List<Carts> findByOrderIdIsNull();

  public List<Carts> findByOrderIdIsNullAndUserId(UserDetails userDetails);

  public Optional<Carts> findByIdAndOrderIdIsNullAndUserId(Integer id, UserDetails userDetails);

  List<Carts> findByOrderIdIsNotNullAndStatus(OrderedFoodStatus status);
}
