package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.MenuCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuCategoryRepository extends JpaRepository<MenuCategory, Integer> {

  public List<MenuCategory> findByStatusTrue();

  public List<MenuCategory> findByCategoryIdIsNull();

}
