package com.vedas.restaurant.dao.projection;

public interface IDs {

  int getId();

  long getUpdateDate();

}
