package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.PhoneNumbers;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumbers, String> {

    public Optional<PhoneNumbers> findByPhoneNumberAndVerificationCodeAndSignupDateBetween(String username, int verificationCode, Long min, Long max);

}
