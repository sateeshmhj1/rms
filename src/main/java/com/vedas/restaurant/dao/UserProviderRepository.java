package com.vedas.restaurant.dao;

import com.vedas.restaurant.database.UserProvider;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProviderRepository extends JpaRepository<UserProvider, Integer> {
      public Optional<UserProvider> findByProviderId(String userId);
}
